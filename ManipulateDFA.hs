module ManipulateDFA
where

import DFA
import Data.Char (isDigit, isLower)
import Data.List (sort, nub, (\\), intersect, union)
import Data.Maybe (fromJust)

{------------------------------------------------------------------------
    Nelson Kai Miin Chen (743322)
    The University of Melbourne
    Octerber 2016
------------------------------------------------------------------------}

--  Keep lists sorted and without duplicates.

tidy :: Ord a => [a] -> [a]
tidy xs
  = nub (sort xs)


--  Calculate the set of reachable states in a given DFA.

reachable :: DFA -> [State]
reachable (states, alphabet, delta, start_state, accept_states)
  = new
    where
      (old, new) = until stable explore ([], [start_state])
      explore (old_reach, cur_reach) = (cur_reach, expand cur_reach)
      expand reach = tidy (reach ++ successors reach)
      successors reach = [y | ((x,_),y) <- delta, x `elem` reach]
      stable (xs, ys) = xs == ys


--  Calculate the set of generating states in a given DFA.

generating :: DFA -> [State]
generating (states, alphabet, delta, start_state, accept_states)
  = new
    where
      (old, new) = until stable explore ([], accept_states)
      explore (old_reach, cur_reach) = (cur_reach, expand cur_reach)
      expand reach = tidy (reach ++ predecessor reach)
      predecessor reach = [x | ((x,_),y) <- delta, y `elem` reach]
      stable (xs, ys) = xs == ys

      -- HINT: FOLLOW THE IDEA IN "REACHABLE"
      -- The idea is to reserve "reachable"

--  Trim a DFA, that is, keep only reachable, generating states
--  (the start state should always be kept).

trim :: DFA -> DFA
trim dfa
  = new
    where
      new =  (new_states, alphabet, trimmed, start_state, new_accept)
      (states, alphabet, delta, start_state, accept_states) = dfa
      generate = (generating dfa) `union` [start_state]
      new_states = (reachable dfa) `intersect` generate
      new_accept = [ a | a <- accept_states,
                     a `elem` new_states]
      trimmed = [ ((x,t),y) | ((x,t),y) <- delta,
                              x `elem` new_states,
                              y `elem` new_states]

-------------------------------------------------------------------------

--  Complete a DFA, that is, make all transitions explict.  For a DFA,
--  the transition function is always understood to be total.

complete :: DFA -> DFA
complete dfa
  = new
    where --- Can change to if ... then ... else ... where ...
      new =  (new_states, alphabet, completed, start_state, accept_states)
      (states, alphabet, delta, start_state, accept_states) = dfa
      all_states = [(s,a) | s <- states, a <- alphabet]
      d_states = [(s,a) | ((s,a),_) <- delta ]
      reject_state = succ(last states)
      new_states =
        if (null (all_states \\ d_states))
          then states
          else states ++ [reject_state]
      completed =
        if (null (all_states \\ d_states))
          then delta
          else delta ++ rejected ++ reject_case
            where
             filtered = [ a | a <- all_states, a `notElem` d_states ]
             rejected = [ ((x,t), reject_state) | (x,t) <- filtered]
             reject_case = [ ((reject_state,t), reject_state) | t <- alphabet]

-------------------------------------------------------------------------

--  Systematically replace the names of states in a DFA with 1..n.

normalise :: DFA -> DFA
normalise dfa
  = new
    where
      new = (new_states, alphabet, normalised, new_start, new_accept)
      (states, alphabet, delta, start_state, accept_states) = dfa
      new_states = [1..length states]
      pair = zip states new_states
      update u = fromJust(lookup u pair)
      new_start = update start_state
      new_accept = [update a | a <- accept_states]
      normalised = [ ((update x,t),update y) | ((x,t),y) <- delta]

-------------------------------------------------------------------------

--  To complete and then normalise a DFA:

full :: DFA -> DFA
full
  = normalise . complete


--  For a given DFA d, generate a DFA d' so that the languages of d
--  and d' are complementary.

complement :: DFA -> DFA
complement dfa
  = new
    where
      new = (states, alphabet, delta, start_state, new_accept)
      (states, alphabet, delta, start_state, accept_states) = completed
      new_accept = [x | x <- states, x `notElem` accept_states]
      completed = full dfa

-------------------------------------------------------------------------

--  Given DFAs d1 and d2, generate a DFA for the intersection of the
--  languages recognised by d1 and d2.

prod :: DFA -> DFA -> DFA
prod dfa1 dfa2
  = new
    where
      fdfa1 = full dfa1
      fdfa2 = full dfa2
      new = (states, alphabet, delta, start_state, accept_states)
      (states1, alphabet1, delta1, start_state1, accept_states1) = fdfa1
      (states2, alphabet2, delta2, start_state2, accept_states2) = fdfa2
      length_s2 = length states2
      length_s1 = length states1
      no_states = (length_s1)*(length_s2)
      states = [1..no_states]
      alphabet = alphabet1 `union` alphabet2
      delta = [((((x1-1)*(length_s2) + x2), t1), ((y1-1)*(length_s2) + y2))
               | ((x1,t1),y1) <- delta1,
                 ((x2,t2),y2) <- delta2,
                 t1==t2]
      start_state = (start_state1 - 1) * length_s2 + start_state2
      accept_states = [ (a-1)*(length_s2) + b | a <- accept_states1,
                                                b <- accept_states2]

-------------------------------------------------------------------------

--  Here is an example (trimmed) DFA; it recognises a*ab*c*

dex :: DFA
dex
  = ([0,1,2,3], "abc", t1, 0, [1,2,3])
    where
      t1 = [ ((0,'a'), 1)
           , ((1,'a'), 1)
           , ((1,'b'), 2)
           , ((1,'c'), 3)
           , ((2,'b'), 2)
           , ((2,'c'), 3)
           , ((3,'c'), 3)
           ]

test1 :: DFA
test1
  = ([0,1,2,3], "abc", t1, 0, [1])
    where
      t1 = [ ((0,'a'), 1)
           , ((1,'b'), 1)
           , ((0,'b'), 2)
           , ((0,'c'), 2)
           , ((1,'a'), 2)
           , ((1,'c'), 2)
           , ((2,'a'), 2)
           , ((2,'b'), 2)
           , ((2,'c'), 2)
           , ((3,'a'), 1)
           , ((3,'b'), 3)
           , ((3,'c'), 2)
           ]

test2 :: DFA
test2
  = ([0,1,2,3,4], "abc", t1, 0, [2,3])
    where
      t1 = [ ((0, 'a'), 1)
           , ((1, 'b'), 2)
           , ((2, 'c'), 2)
           , ((2, 'a'), 2)
           , ((1, 'c'), 3)
           , ((3, 'b'), 3)
           , ((3, 'a'), 3)
           , ((3, 'c'), 4)
           , ((4, 'a'), 4)
           , ((4, 'b'), 4)
           , ((2, 'b'), 4)
           ]

test3 :: DFA
test3
 = ([0,1,2,3], "abc", t1, 0, [2])
   where
     t1 = [ ((0, 'a'), 1)
          , ((1, 'a'), 1)
          , ((1, 'b'), 1)
          , ((1, 'c'), 1)
          , ((1, 'b'), 2)
          , ((0, 'a'), 3)
          , ((3, 'b'), 3)
          , ((3, 'c'), 3)
          , ((3, 'b'), 2)
          ]

test4 :: DFA
test4
  = ([1,2], "abc", t1, 1, [2])
    where
      t1 = [ ((1,'a'),2)
           , ((2,'b'),2)]


q1 :: DFA
q1
  = ([1,2],"abc", t1, 1, [1])
    where
      t1 = [ ((1,'a'),2)
           , ((2,'b'),2)
           , ((2,'a'),1)
           , ((1,'b'),1)
           ]

q2 :: DFA
q2
 = ([1,2,3,4],"abc", t1, 1, [2,3])
    where
       t1 = [ ((1,'a'),1)
            , ((1,'b'),2)
            , ((2,'a'),2)
            , ((2,'b'),3)
            , ((3,'a'),3)
            , ((3,'b'),4)
            , ((4,'a'),4)
            , ((4,'b'),4)
            ]

-------------------------------------------------------------------------
